# WellSpring Leadership

This is the body of leadership knowledge that supports the use of the Spring Product Lab Framework.

# Intentions

The intent of this repository is to house the leadership and co-creator handbooks that provide the actionable culture,leadership and managment framework of the Spring Product Lab.

# Resources
Important note.  Our WellSpring Leadership content is open-source and does draw content of others which may not be open source. These others own their content, all rights reserved, and we recommend that any person seeking to deepen their skills turn to those resources as primary training. We have made every effort to cite our sources and direct our community members to utilize the thought leadership, and Copywrite protected materials from their source.
