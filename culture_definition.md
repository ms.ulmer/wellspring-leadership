Culture is **how we behave** among others **while we co-create.**

It is also how **we choose to speak about behave towards** our colleagues **in their absence.**

**Be kind** when we feel taxed and **exercising tolerance and boundaries** where needed. **Inviting and accepting invitations** to the Dojo to **collaborate on resolving struggle** through safe listening, learning, and reflecting.

**Kind communication is timely, clear, respectful with our counterpart(s)**.

Unkind communication is unclear, avoidant, backchanneled through others, and/or not respectful.

**We celebrate our victories**.

**We also celebrate and examine our failures** for the rich learning they offer.

Culture is not:

- A set of slogans
- An outfit/uniform
- An externally imposed belief system from a leadership team, HR department, or another external group from the team.
